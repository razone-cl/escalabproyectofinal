
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookService } from '../../../components/books/book.service';
import { BookI } from '../../models/book.interface';

import Swal from 'sweetalert2';

import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './../modal/modal.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['titleBook','descriptionBook', 'tagsBook', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private bookSvc: BookService, public dialog: MatDialog) { }

  ngOnInit() {
    this.bookSvc
      .getAllBooks()
      .subscribe(books => (this.dataSource.data = books));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onEditBook(book: BookI) {
    console.log('Edit book', book);
    this.openDialog(book);
  }

  onDeleteBook(book: BookI) {
    Swal.fire({
      title: 'Estás Seguro?',
      text: `Esta acción es irreversible`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'BORRAR'
    }).then(result => {
      if (result.value) {
        this.bookSvc.deleteBookById(book).then(() => {
          Swal.fire('Book Borrado!', 'Tu book fue borrado.', 'success');
        }).catch((error) => {
          Swal.fire('Error!', 'Hubo un error borrando este book', 'error');
        });
      }
    });

  }

  onNewBook() {
    this.openDialog();
  }

  openDialog(book?: BookI): void {
    const config = {
      data: {
        message: book ? `Editar Book ${book.titleBook}` : 'Nuevo Book',
        content: book
      }
    };

    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result ${result}`);
    });
  }
}
