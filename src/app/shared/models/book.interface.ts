export interface BookI {
    titleBook: string;
    descriptionBook: string;
    imageBook?: any;
    id?: string;
    tagsBook: string;
    fileRef?: string;
  }
  