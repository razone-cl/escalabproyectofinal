import { BookService } from './../../books/book.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs'
import { BookI } from '../../../shared/models/book.interface'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public books$: Observable<BookI[]>; //un array de book

  constructor(private bookSvc: BookService) { }

  ngOnInit() {
    this.books$ = this.bookSvc.getAllBooks();
  }
}
