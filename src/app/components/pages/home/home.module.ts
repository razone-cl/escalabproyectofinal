import { BookComponent } from './../../books/book/book.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MaterialModule } from '../../../material.module';

@NgModule({
  declarations: [HomeComponent, BookComponent],
  imports: [CommonModule, HomeRoutingModule, MaterialModule]
})
export class HomeModule { }
