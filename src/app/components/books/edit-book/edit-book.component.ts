import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BookI } from '../../../shared/models/book.interface';
import { BookService } from './../book.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {
  private image: any;
  private imageOriginal: any;

  @Input() book: BookI;

  constructor(private bookSvc: BookService) { }

  public editBookForm = new FormGroup({
    id: new FormControl('', Validators.required),
    titleBook: new FormControl('', Validators.required),
    descriptionBook: new FormControl('', Validators.required),
    tagsBook: new FormControl('', Validators.required),
    imageBook: new FormControl('', Validators.required),
  });

  ngOnInit() {
    this.image = this.book.imageBook;
    this.imageOriginal = this.book.imageBook;
    this.initValuesForm();
  }

  editBook(book: BookI) {
    if (this.image === this.imageOriginal) {
      book.imageBook = this.imageOriginal;
      this.bookSvc.editBookById(book);
    } else {
      this.bookSvc.editBookById(book, this.image);
    }
  }

  handleImage(event: any): void {
    this.image = event.target.files[0];
  }

  private initValuesForm(): void {
    this.editBookForm.patchValue({
      id: this.book.id,
      titleBook: this.book.titleBook,
      descriptionBook: this.book.descriptionBook,
      tagsBook: this.book.tagsBook
    });
  }

}
