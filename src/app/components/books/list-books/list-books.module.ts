import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListBooksRoutingModule } from './list-books-routing.module';
import { ListBooksComponent } from './list-books.component';
import { MaterialModule } from '../../../material.module';
import { TableComponent } from '../../../shared/components/table/table.component';

@NgModule({
  declarations: [ListBooksComponent, TableComponent],
  imports: [CommonModule, ListBooksRoutingModule, MaterialModule]
})
export class ListBooksModule {}
