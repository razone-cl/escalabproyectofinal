import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BookI } from '../../../shared/models/book.interface';
import { BookService } from '../book.service';

@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.scss']
})
export class NewBookComponent implements OnInit {
  private image: any;
  constructor(private bookSvc: BookService) { }

  public newBookForm = new FormGroup({
    titleBook: new FormControl('', Validators.required),
    descriptionBook: new FormControl('', Validators.required),
    tagsBook: new FormControl('', Validators.required),
    imageBook: new FormControl('', Validators.required),
  });

  ngOnInit() {
  }

  addNewBook(data: BookI) {
    console.log('New book', data);
    this.bookSvc.preAddAndUpdateBook(data, this.image);
  }

  handleImage(event: any): void {
    this.image = event.target.files[0];
  }
}
