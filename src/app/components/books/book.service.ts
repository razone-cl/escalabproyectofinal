import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'; //trabajar collections con angular_firebase
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
//map recorre una data y el contenido lo puede modificar
//finalize: describe que se ha terminado un proceso de un observable
import { BookI } from '../../shared/models/book.interface';
import { FileI } from '../../shared/models/file.interface';
import { AngularFireStorage } from '@angular/fire/storage';
//para almacenar archivos/imagenes
@Injectable({
  providedIn: 'root'
})
export class BookService {
  private booksCollection: AngularFirestoreCollection<BookI>;
  private filePath: any;
  private downloadURL: Observable<string>;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {
    this.booksCollection = afs.collection<BookI>('books');
    //conectar angular a firebase y esa va a mi bdd en firebase
  }

  public getAllBooks(): Observable<BookI[]> {
    return this.booksCollection
      .snapshotChanges() //guardo los cambios instantaneos de mi colect
      .pipe( //observable asociado a varias funcionalidades
        map(actions => //map me permite recorrer la info y poder modificar su estructura
          actions.map(a => {
            const data = a.payload.doc.data() as BookI; //payload es todo el cuerpo de la info, doc es la info afectada por los cambios, data te devuelve todo el contenido como un objeto
            const id = a.payload.doc.id;
            return { id, ...data }; //toda la data de cada book con su id, unificamos esa info
          })
        )
      );
  }

  public getOneBook(id: BookI): Observable<BookI> {
    return this.afs.doc<BookI>(`books/${id}`).valueChanges(); //retornando los cambios de los valores del elemento afectado
  }

  public deleteBookById(book: BookI) {
    return this.booksCollection.doc(book.id).delete();
  }

  public editBookById(book: BookI, newImage?: FileI) {
    if (newImage) {
      this.uploadImage(book, newImage);
    } else {
      return this.booksCollection.doc(book.id).update(book);
    }
  }

  public preAddAndUpdateBook(book: BookI, image: FileI): void {
    this.uploadImage(book, image);
  }

  private saveBook(book: BookI) {
    const bookObj = {
      titleBook: book.titleBook,
      descriptionBook: book.descriptionBook,
      imageBook: this.downloadURL,
      fileRef: this.filePath,
      tagsBook: book.tagsBook
    };

    if (book.id) { //cuando guardo algún documento ya existente,ej:despues de un cambio
      return this.booksCollection.doc(book.id).update(bookObj);
    } else { //cuando agrego una nueva
      return this.booksCollection.add(bookObj);
    }

  }

  private uploadImage(book: BookI, image: FileI) {
    this.filePath = `images/${image.name}`; //referencia de la ruta de la imagen
    const fileRef = this.storage.ref(this.filePath);
    //referenciar la imagen con su path en firebase storage
    const task = this.storage.upload(this.filePath, image);
    //ya la subo en el storage de firebase
    task.snapshotChanges()
      .pipe(
        finalize(() => { //termina el proceso luego de que ya se haya 
          //guardado la imagen en storage y haya guardado el book
          fileRef.getDownloadURL().subscribe(urlImage => {
            this.downloadURL = urlImage;
            this.saveBook(book);
          });
        })
      ).subscribe();
  }
}
