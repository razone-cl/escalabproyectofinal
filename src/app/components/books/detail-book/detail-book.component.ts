import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from '../book.service';
import { Observable } from 'rxjs';
import { BookI } from '../../../shared/models/book.interface';

@Component({
  selector: 'app-detail-book',
  templateUrl: './detail-book.component.html',
  styleUrls: ['./detail-book.component.scss']
})
export class DetailBookComponent implements OnInit {
  public book$: Observable<BookI>;

  constructor(private route: ActivatedRoute, private bookSvc: BookService) { }

  ngOnInit() {
    const idBook = this.route.snapshot.params.id; //devuelve un objeto con el parametro capturado, el params solito devuelve un observable
    this.book$ = this.bookSvc.getOneBook(idBook); //obtengo el book para ese id recuperado de la url
  }

}
