import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../../books/book.service';
import { BookI } from '../../../shared/models/book.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  // public books$: Observable<BookI[]>;
  @Input() book: BookI;

  constructor(private bookSvc: BookService) { }

  ngOnInit() {
    // this.books$ = this.bookSvc.getAllBooks();
  }
}
