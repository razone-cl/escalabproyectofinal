import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//COMPONENTS Y RUTAS
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewBookModule } from './components/books/new-book/new-book.module';
import { NewBookComponent } from './components/books/new-book/new-book.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToolbarComponent } from './shared/components/toolbar/toolbar.component';
//FIREBASE
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage'; //gestionar la subida de imagenes
import { AngularFireModule } from '@angular/fire'; //funciones o metodos de firebase
import { AngularFireAuthModule } from '@angular/fire/auth';
//trabajar con la autenticacion de firebase

import { environment } from '../environments/environment';

import { ReactiveFormsModule } from '@angular/forms';

//para trabajar con forms reactivos con validaciones y sus directivas
import { ContainerAppComponent } from './components/pages/container-app/container-app.component';
import { ModalComponent } from './shared/components/modal/modal.component';
import { DetailBookComponent } from './components/books/detail-book/detail-book.component';
import { EditBookComponent } from './components/books/edit-book/edit-book.component';
import { EditBookModule } from './components/books/edit-book/edit-book.module';



@NgModule({
  declarations: [
    AppComponent,
    NewBookComponent,
    ToolbarComponent,
    ContainerAppComponent,
    ModalComponent,
    EditBookComponent,
    DetailBookComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AppRoutingModule,
    MaterialModule,
    NewBookModule,
    ReactiveFormsModule,
    EditBookModule

  ],
  entryComponents: [ModalComponent],
  providers: [
    { provide: StorageBucket, useValue: 'gs://photobook-4df8a.appspot.com' } //le pasamos la cadena del storage bucket de firebase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
